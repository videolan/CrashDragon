const FormatDateWithTime = function (ISODate: string) {
    return new Date(ISODate).toLocaleString("default", {
        year: "numeric",
        month: "long",
        day: "numeric",
        hour: "2-digit",
        minute: "2-digit",
    });
};

export { FormatDateWithTime };
