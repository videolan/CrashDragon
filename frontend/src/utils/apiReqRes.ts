export interface ListApiResponse<T> {
    ItemCount: number;
    Items: T[];
    Limit: number;
    Offset: number;
}

export interface ApiResponse<T> {
    Error: string;
    Item: T;
}
