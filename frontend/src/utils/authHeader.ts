import { useAuthStore } from "../store/AuthStore";

const authStore = useAuthStore();
const currentUser = authStore.currentUser;

export function authHeader() {
    // return authorization header with basic auth credentials
    if (currentUser && currentUser.Name) {
        console.log("currentUser.Name", currentUser.Name);
        console.log("btoa(currentUser.Name)", btoa(currentUser.Name));
        return { Authorization: "Basic " + btoa(currentUser.Name) };
    }
    return {};
}
