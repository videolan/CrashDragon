import { toast } from "vue3-toastify";
import "vue3-toastify/dist/index.css";

const toastOptions = {
    position: "top-right",
    autoClose: 3000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
    theme: "colored",
};

export { toast, toastOptions };
