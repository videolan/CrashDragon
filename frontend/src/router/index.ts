import { createRouter, createWebHistory } from "vue-router";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.min.js";
import AdminLayout from "../layouts/AdminLayout.vue";
import AdminLogin from "../views/AdminLogin.vue";
import HomeLayout from "../layouts/HomeLayout.vue";
import { ROUTES } from "./constants";
import CrashPage from "../views/CrashPage.vue";
import SymfilesPage from "../views/SymfilesPage.vue";
import ReportsPage from "../views/ReportsPage.vue";
import HomePage from "../views/HomePage.vue";
import VersionsPage from "../views/VersionPage.vue";
import { useAuthStore } from "../store/AuthStore";

const routes = [
    {
        path: "/admin",
        component: AdminLayout,
        children: [
            {
                path: "/admin/login",
                name: ROUTES.LOGIN,
                component: AdminLogin,
            },
        ],
    },
    {
        path: "/",
        component: HomeLayout,
        children: [
            {
                path: "/",
                name: ROUTES.HOME,
                component: HomePage,
            },
            {
                path: "/reports",
                name: ROUTES.REPORTS,
                component: ReportsPage,
            },
            {
                path: "/crash",
                name: ROUTES.CRASH,
                component: CrashPage,
            },
            {
                path: "/symfiles",
                name: ROUTES.SYMFILES,
                component: SymfilesPage,
            },
            {
                path: "/versions/:id",
                name: ROUTES.VERSION,
                component: VersionsPage,
                props: true,
            },
        ],
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

router.beforeEach((to, _) => {
    const authStore = useAuthStore();

    if (to.name === ROUTES.LOGIN && authStore.currentUser) {
        return { name: ROUTES.HOME };
    }

    if (to.name !== ROUTES.LOGIN && !authStore.currentUser) {
        return { name: ROUTES.LOGIN };
    }

    return true;
});

export default router;
