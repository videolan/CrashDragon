export const ROUTES = {
    HOME: "home",
    LOGIN: "login",
    REPORTS: "reports",
    CRASH: "crash",
    SYMFILES: "symfiles",
    VERSION: "versions",
};
