import { defineStore } from "pinia";
import axiosClient from "../utils/axiosClient";
import { ApiResponse, ListApiResponse } from "../utils/apiReqRes";

export interface Product {
    ID: string;
    CreatedAt: string;
    UpdatedAt: string;
    Name: string;
    Slug: string;
}

// Define the state externally
function initialState() {
    return {
        products: [] as Product[],
        loading: false,
        error: null,
    };
}

async function fetchProducts(this: ReturnType<typeof useProductsStore>) {
    this.loading = true;
    this.error = null;
    try {
        const response = await axiosClient.get<ListApiResponse<Product>>("/products");
        if (response.status !== 200) {
            throw new Error("Failed to fetch products with status: " + response.status);
        }

        this.products = response.data.Items;
    } catch (error: any) {
        console.log("Failed to fetch products", error.message);
        this.error = error.message || "Failed to fetch products";
    } finally {
        this.loading = false;
    }
}

async function fetchProductById(this: ReturnType<typeof useProductsStore>, id: string) {
    this.loading = true;
    this.error = null;
    try {
        const encodedId = encodeURIComponent(id);
        const response = await axiosClient.get<ApiResponse<Product>>(`/products/${encodedId}`);

        if (response.status !== 200) {
            throw new Error("Failed to fetch product with status: " + response.status);
        }

        this.products.push(response.data.Item);
    } catch (error: any) {
        console.log("Failed to fetch product", error.message);
        this.error = error.message || "Failed to fetch product";
    } finally {
        this.loading = false;
    }
}

async function createProduct(this: ReturnType<typeof useProductsStore>, newProduct: Product) {
    try {
        const response = await axiosClient.post("/products", newProduct);
        if (response.status !== 201) {
            throw new Error("Failed to create product with status: " + response.status);
        }
        this.products.push(response.data);
    } catch (error: any) {
        console.log("Failed to create product", error.message);
        this.error = error.message || "Failed to create product";
    }
}

async function deleteProduct(this: ReturnType<typeof useProductsStore>, id: string) {
    try {
        const encodedId = encodeURIComponent(id);
        const response = await axiosClient.delete(`/products/${encodedId}`);
        if (response.status !== 200) {
            throw new Error("Failed to delete product with status: " + response.status);
        }
        this.products = this.products.filter((product) => product.ID !== id);
    } catch (error: any) {
        console.log("Failed to delete product", error.message);
        this.error = error.message || "Failed to delete product";
    }
}

async function updateProduct(this: ReturnType<typeof useProductsStore>, updatedProduct: Product) {
    try {
        const encodedProductId = encodeURIComponent(updatedProduct.ID);
        const response = await axiosClient.put(`/products/${encodedProductId}`, updatedProduct);
        if (response.status !== 200) {
            throw new Error("Failed to update product with status: " + response.status);
        }
        const index = this.products.findIndex((product) => product.ID === updatedProduct.ID);
        if (index !== -1) {
            this.products.splice(index, 1, response.data);
        }
    } catch (error: any) {
        console.log("Failed to update product", error.message);
        this.error = error.message || "Failed to update product";
    }
}

function allProducts(state: ReturnType<typeof initialState>) {
    return state.products;
}

function allProductsState(state: ReturnType<typeof initialState>) {
    return state;
}

function getProductById(state: ReturnType<typeof initialState>) {
    return (id: string) => state.products.find((product) => product.ID === id);
}

export const useProductsStore = defineStore("products", {
    state: initialState,

    actions: {
        fetchProducts,
        fetchProductById,
        createProduct,
        deleteProduct,
        updateProduct,
    },

    getters: {
        allProducts,
        getProductById,
        allProductsState,
    },
});
