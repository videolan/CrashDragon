import { defineStore } from "pinia";
import axiosClient from "../utils/axiosClient";
import { ListApiResponse, ApiResponse } from "../utils/apiReqRes";

export interface Version {
    ID: string;
    CreatedAt: string;
    UpdatedAt: string;
    Name: string;
    Slug: string;
    GitRepo: string;
    Ignore: boolean;
    ProductID: string;
}

export interface UpdateVersion {
    Name: string;
    Slug: string;
    GitRepo: string;
    Ignore: boolean;
    ProductID: string;
}

// Define the state externally
function initialState() {
    return {
        versions: [] as Version[],
        loading: false,
        error: null,
        updateLoading: false,
    };
}

async function fetchVersions(this: ReturnType<typeof useVersionsStore>) {
    this.loading = true;
    this.error = null;
    try {
        const response = await axiosClient.get<ListApiResponse<Version>>("/versions");
        if (response.status !== 200) {
            throw new Error("Failed to fetch versions with status: " + response.status);
        }

        const versions = response.data.Items;

        this.versions = versions;
    } catch (error: any) {
        console.log("Failed to fetch versions", error.message);
        this.error = error.message || "Failed to fetch versions";
    } finally {
        this.loading = false;
    }
}

async function fetchVersionById(this: ReturnType<typeof useVersionsStore>, id: string) {
    this.loading = true;
    this.error = null;
    try {
        if (this.versions.find((v) => v.ID === id)) {
            return;
        }

        const encodedId = encodeURIComponent(id);
        const response = await axiosClient.get<ApiResponse<Version>>(`/versions/${encodedId}`);
        const fetchedVersion = response.data.Item;

        this.versions.push(fetchedVersion);
    } catch (error: any) {
        console.log("Failed to fetch version", error.message);
        this.error = error.message || "Failed to fetch version";
    } finally {
        this.loading = false;
    }
}

async function updateVersionById(
    this: ReturnType<typeof useVersionsStore>,
    versionId: string,
    updateVersionData: UpdateVersion,
) {
    this.updateLoading = true;
    this.error = null;
    try {
        const encodedId = encodeURIComponent(versionId);
        const response = await axiosClient.put<ApiResponse<Version>>(`/versions/${encodedId}`, updateVersionData);
        const updatedVersion = response.data.Item;

        this.versions = this.versions.map((version) => (version.ID === updatedVersion.ID ? updatedVersion : version));
    } catch (error: any) {
        console.log("Failed to update version", error.message);
        this.error = error.message || "Failed to update version";
    } finally {
        this.updateLoading = false;
    }
}

function allVersions(state: ReturnType<typeof initialState>) {
    return state.versions;
}

function allVersionsState(state: ReturnType<typeof initialState>) {
    return state;
}

function getVersionById(state: ReturnType<typeof initialState>) {
    return (id: string) => state.versions.find((version) => version.ID === id);
}

export const useVersionsStore = defineStore("versions", {
    state: initialState,

    actions: {
        fetchVersions,
        fetchVersionById,
        updateVersionById,
    },

    getters: {
        allVersions,
        getVersionById,
        allVersionsState,
    },
});
