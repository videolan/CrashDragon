import { defineStore } from "pinia";
import axiosClient from "../utils/axiosClient";
import { ApiResponse, ListApiResponse } from "../utils/apiReqRes";
import { Comment } from "vue";

export interface Comment {
    ID: string;
    CreatedAt: string;
    UpdatedAt: string;
    CrashID: string;
    ReportID: string;
    UserID: string;
    Content: string;
}

export interface NewComment {
    CrashID: string;
    ReportID: string;
    UserID: string;
    Content: string;
}

// Define the state externally
function initialState() {
    return {
        comments: [] as Comment[],
        loading: false,
        error: null,
        modifyError: null,
    };
}

async function fetchCommentsByCrash(this: ReturnType<typeof useCommentsStore>, crashId: string) {
    this.loading = true;
    this.error = null;
    try {
        const encodedCrashId = encodeURIComponent(crashId);
        const resp = await axiosClient.get<ListApiResponse<Comment>>(`/comments/crash/${encodedCrashId}`);

        if (resp.status !== 200) {
            throw new Error("Failed to fetch comments with status: " + resp.status);
        }

        const comments = resp.data.Items;
        this.comments.push(...comments);
    } catch (error: any) {
        console.log("Failed to fetch comments", error.message);
        this.error = error.message || "Failed to fetch comments";
    } finally {
        this.loading = false;
    }
}

async function fetchCommentsByReport(this: ReturnType<typeof useCommentsStore>, reportId: string) {
    this.loading = true;
    this.error = null;
    try {
        const encodedReportId = encodeURIComponent(reportId);
        const resp = await axiosClient.get<ListApiResponse<Comment>>(`/comments/crash/${encodedReportId}`);

        if (resp.status !== 200) {
            throw new Error("Failed to fetch comments with status: " + resp.status);
        }

        const comments = resp.data.Items;
        this.comments.push(...comments);
    } catch (error: any) {
        console.log("Failed to fetch comments", error.message);
        this.error = error.message || "Failed to fetch comments";
    } finally {
        this.loading = false;
    }
}

async function fetchCommentById(this: ReturnType<typeof useCommentsStore>, id: string) {
    this.loading = true;
    this.error = null;
    try {
        const encodedId = encodeURIComponent(id);
        const resp = await axiosClient.get<ApiResponse<Comment>>(`/comments/${encodedId}`);

        if (resp.status !== 200) {
            throw new Error("Failed to fetch comment with status: " + resp.status);
        }

        const comment = resp.data.Item;
        this.comments.push(comment);
    } catch (error: any) {
        console.log("Failed to fetch comment", error.message);
        this.error = error.message || "Failed to fetch comment";
    } finally {
        this.loading = false;
    }
}

async function addComment(this: ReturnType<typeof useCommentsStore>, newComment: NewComment) {
    this.modifyError = null;
    try {
        const resp = await axiosClient.post<ApiResponse<Comment>>("/comments", newComment);

        if (resp.status !== 201) {
            throw new Error("Failed to add comment with status: " + resp.status);
        }

        const comment = resp.data.Item;
        this.comments.push(comment);
    } catch (error: any) {
        console.log("Failed to add comment", error.message);
        this.modifyError = error.message || "Failed to add comment";
    }
}

async function removeComment(this: ReturnType<typeof useCommentsStore>, id: string) {
    this.modifyError = null;
    try {
        const encodedId = encodeURIComponent(id);
        const resp = await axiosClient.delete<ApiResponse<Comment>>(`/comments/${encodedId}`);

        if (resp.status !== 200) {
            throw new Error("Failed to delete comment with status: " + resp.status);
        }

        this.comments = this.comments.filter((comment) => comment.ID !== id);
    } catch (error: any) {
        console.log("Failed to delete comment", error.message);
        this.modifyError = error.message || "Failed to delete comment";
    }
}

function allComments(state: ReturnType<typeof initialState>) {
    return state.comments;
}

function allCommentsState(state: ReturnType<typeof initialState>) {
    return state;
}

function getCommentById(state: ReturnType<typeof initialState>) {
    return (id: string) => state.comments.find((comment) => comment.ID === id);
}

function getCommentByReportId(state: ReturnType<typeof initialState>) {
    return (reportId: string) => state.comments.find((comment) => comment.ReportID === reportId);
}

function getCommentByCrashId(state: ReturnType<typeof initialState>) {
    return (crashId: string) => state.comments.find((comment) => comment.CrashID === crashId);
}

export const useCommentsStore = defineStore("comments", {
    state: initialState,

    actions: {
        fetchCommentsByCrash,
        fetchCommentsByReport,
        fetchCommentById,
        addComment,
        removeComment,
    },

    getters: {
        allComments,
        getCommentById,
        allCommentsState,
        getCommentByReportId,
        getCommentByCrashId,
    },
});

export type CommentStoreState = ReturnType<typeof initialState>;
