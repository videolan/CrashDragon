import { defineStore } from "pinia";

export interface AdminUser {
    ID: string;
    CreatedAt: string;
    UpdatedAt: string;
    Name: string;
    IsAdmin: boolean;
}

function initialState() {
    const user = localStorage.getItem("user");
    if (user !== null) {
        return {
            user: JSON.parse(user) as AdminUser,
            loading: false,
            error: null,
        };
    }

    console.log("User not found in local storage");
    return {
        user: null as AdminUser | null,
        loading: false,
        error: null,
    };
}

async function loginUser(this: ReturnType<typeof useAuthStore>, username: string, password: string) {
    this.loading = true;
    this.error = null;
    try {
        console.log("Logging in with", username, password);

        // const authHeader = "Basic " + btoa(username)
        // console.log('authHeader', authHeader)

        // const response = await axiosClient.get<ApiResponse>('/users', { headers: { Authorization: authHeader } });

        // if (response.data.Error) {
        //     throw new Error(response.data.Error);
        // }

        // const user = response.data.Item;

        // ? Dummy user
        const user: AdminUser = {
            ID: "10f8b432-6b03-4e4c-8f70-203ada5433cb",
            CreatedAt: "2024-06-18 18:07:07.643309+00",
            UpdatedAt: "2024-06-18 18:07:07.643309+00",
            Name: "Achintya",
            IsAdmin: true,
        };

        // also store the user in local storage
        localStorage.setItem("user", JSON.stringify(user));

        this.user = user;
    } catch (error: any) {
        console.log("Failed to login", error.message);
        this.error = error.message || "Failed to login";
    } finally {
        this.loading = false;
    }
}

async function logoutUser(this: ReturnType<typeof useAuthStore>) {
    this.loading = true;
    this.error = null;
    try {
        // remove the user from local storage
        localStorage.removeItem("user");

        this.user = null;
    } catch (error: any) {
        console.log("Failed to logout", error.message);
        this.error = error.message || "Failed to logout";
    } finally {
        this.loading = false;
    }
}

function isAdmin(this: ReturnType<typeof initialState>) {
    return this.user?.IsAdmin || false;
}

function currentUser(this: ReturnType<typeof initialState>) {
    return this.user;
}

function authState(state: ReturnType<typeof initialState>) {
    return state;
}

export const useAuthStore = defineStore("auth", {
    state: initialState,

    actions: {
        loginUser,
        logoutUser,
    },

    getters: {
        isAdmin,
        currentUser,
        authState,
    },
});

export type AuthStore = ReturnType<typeof useAuthStore>;
