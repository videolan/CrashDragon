import { defineStore } from "pinia";
import axiosClient from "../utils/axiosClient";
import { ApiResponse } from "../utils/apiReqRes";

export interface User {
    id: string;
    name: string;
    isAdmin: boolean;
    createdAt: Date;
    updatedAt: Date;
}

function initialState() {
    return {
        user: null as User | null,
        loading: false,
        error: null,
    };
}

async function fetchUser(this: ReturnType<typeof useUserStore>, id: string) {
    this.loading = true;
    this.error = null;
    try {
        const encodedId = encodeURIComponent(id);
        const resp = await axiosClient.get<ApiResponse<User>>(`/users/${encodedId}`);

        if (resp.status !== 200) {
            throw new Error("Failed to fetch user with status: " + resp.status);
        }

        this.user = resp.data.Item;
    } catch (error: any) {
        console.log("Failed to fetch user", error.message);
        this.error = error.message || "Failed to fetch user";
    } finally {
        this.loading = false;
    }
}

function getUser(this: ReturnType<typeof initialState>) {
    return this.user;
}

export const useUserStore = defineStore("user", {
    state: initialState,

    actions: {
        fetchUser,
    },

    getters: {
        getUser,
    },
});
