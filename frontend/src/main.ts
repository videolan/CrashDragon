import { createApp } from "vue";
import { createPinia } from "pinia";
import "./style.css";
import router from "./router";
import Vue3Toastify, { type ToastContainerOptions } from "vue3-toastify";
import App from "./App.vue";

const toastConfig: ToastContainerOptions = {
    position: "bottom-right",
    autoClose: 2000,
    hideProgressBar: false,
    closeOnClick: true,
    pauseOnHover: true,
    theme: "colored",
};

createApp(App).use(createPinia()).use(Vue3Toastify, toastConfig).use(router).mount("#app");
